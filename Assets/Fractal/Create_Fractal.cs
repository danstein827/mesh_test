﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Create_Fractal : MonoBehaviour {
    public Mesh mesh;
    public Material material;
    public Material[] materials;
    public Gradient gradient;

    int current_Depth = 0;
    public int max_Depth = 4;
    public float child_Size = 1f;

    Vector3[] vector3_Array = new Vector3[] {
        Vector3.up,
        Vector3.down,
        Vector3.left,
        Vector3.right,
        Vector3.forward,
        Vector3.back };

    Quaternion[] quaternion_Array = {
        Quaternion.identity,
        Quaternion.Euler(180f, 0f, 0f),
        Quaternion.Euler(0f, 90f, 0f),
        Quaternion.Euler(0f, -90f, 0f),
        Quaternion.Euler(0f, 0f, 90f),
        Quaternion.Euler(0f, 0f, -90f)};


    public Vector3 direction = Vector3.zero;
    public Quaternion rotation = Quaternion.identity;

    private void initialize(Create_Fractal parent, int _direction)
    {
        current_Depth = parent.current_Depth + 1;
        max_Depth = parent.max_Depth;

        mesh = parent.mesh;
        material = parent.material;
        materials = parent.materials;

        transform.parent = parent.transform;
        child_Size = parent.child_Size;

        direction = vector3_Array[_direction];
        rotation = quaternion_Array[_direction];

        transform.localScale = Vector3.one * child_Size;
        transform.localPosition = direction * (.5f + .5f * child_Size);
        //transform.rotation = rotation;
    }

    // Use this for initialization
    void Start () {
        if(materials.Length == 0)
        {
            initilize_Materials();
        }

        gameObject.AddComponent<MeshFilter>().mesh = mesh;
        gameObject.AddComponent<MeshRenderer>().material = materials[current_Depth];

		if(current_Depth < max_Depth)
        {
            
            for (int i = 0; i < vector3_Array.Length; i++)
            {
                if(vector3_Array[i] != -direction)
                {
                    new GameObject("FractaclChild").AddComponent<Create_Fractal>().initialize(this, i);
                }
            }
        }

        if(current_Depth == 0)
        {
            direction = Vector3.up;
        }
	}

    private void initilize_Materials()
    {
        materials = new Material[max_Depth + 1];

        for(int i = 0; i <= max_Depth; i++)
        {
            materials[i] = new Material(material);
            materials[i].color = gradient.Evaluate((float)i / (max_Depth));

        }
    }

    // Update is called once per frame
    void Update () {
        transform.Rotate(direction, 2f * current_Depth + 1);
	}
}
