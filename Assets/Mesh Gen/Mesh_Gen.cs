﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
public class Mesh_Gen : MonoBehaviour {
    [SerializeField] int xSize, ySize;
    private Vector3[] verticies;
    private Mesh mesh;

    private void Awake()
    {
        GetComponent<MeshFilter>().mesh = mesh = new Mesh();
        mesh.name = "Procedural Grid";
        generator();
    }

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void generator()
    {
        float x_Off = xSize / 2f, y_Off = ySize / 2f;
        WaitForSeconds wait = new WaitForSeconds(0.05f);


        verticies = new Vector3[(xSize + 1) * (ySize + 1)];
        Vector2[] uv = new Vector2[verticies.Length];
        Vector4[] tangents = new Vector4[verticies.Length];
        Vector4 tangent = new Vector4(1f, 0f, 0f, -1f);

        for(int i = 0, y = 0; y <= ySize; y++)
        {
            for (int x = 0; x <= xSize; x++, i++)
            {
                verticies[i] = new Vector3(x - x_Off, y - y_Off);
                uv[i] = new Vector2((float)x / xSize, (float)y / ySize);
                tangents[i] = tangent;
                
            }
        }

        mesh.vertices = verticies;
        mesh.uv = uv;
        mesh.tangents = tangents;

        int[] triangles = new int[ySize*xSize * 6];

        for(int y = 0, indx = 0, vert = 0; y < ySize; y++, vert++)
        {
            for (int x = 0; x < xSize; x++, vert++, indx += 6)
            {
                triangles[indx] = vert;
                triangles[indx + 1] = triangles[indx + 4] = vert + xSize + 1;
                triangles[indx + 2] = triangles[indx + 3] = vert + 1;
                triangles[indx + 5] = vert + xSize + 2;
                mesh.triangles = triangles;
            }
        }

        mesh.RecalculateNormals();

    }

    private void OnDrawGizmos()
    {
        if (verticies == null)
        {
            return;
        }

        Gizmos.color = Color.black;

        for(int i = 0; i < verticies.Length; i++)
        {
            Gizmos.DrawSphere(verticies[i], .1f);
        }
    }
}
